package com.test.customerservice.model;

public class Seat {

	
	private Integer seatId;
	private String passengerName;
 
	


	//true for male, false for female
	private Boolean gender;
	
//	public Seat() {
//		
//	}

	public Seat() {
		
	}
	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	public Seat(Integer seatId, String passengerName, Boolean gender) {
		super();
		this.seatId = seatId;
		this.passengerName = passengerName;
		this.gender = gender;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public  Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	
	
	
}

package com.test.customerservice.model;

import java.util.List;

public class Bus {

	private Integer busId;
	private String busName;
	private List<Seat> seatlist;
	public Bus() {
		
	}
	public Integer getBusId() {
		return busId;
	}
	public void setBusId(Integer busId) {
		this.busId = busId;
	}
	public List<Seat> getSeatlist() {
		return seatlist;
	}
	public void setSeatlist(List<Seat> seatlist) {
		this.seatlist = seatlist;
	}
	public Bus(Integer busId, String busName) {
		super();
		this.busId = busId;
		this.busName = busName;
	}
	public String getBusName() {
		return busName;
	}
	public void setBusName(String busName) {
		this.busName = busName;
	}
	
	
}

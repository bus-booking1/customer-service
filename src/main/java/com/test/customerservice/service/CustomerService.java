package com.test.customerservice.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.test.customerservice.model.Bus;
import com.test.customerservice.model.Customer;

@Service
public class CustomerService {

	
	List<Customer> customerList = new ArrayList<>(Arrays.asList(
			new Customer(1,"customer1","9876543210"),
			new Customer(2,"customer2","8765432190"),
			new Customer(3,"customer3","7654321890")
			));
	
	@Autowired
	private RestTemplate restTemplate;
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

	
	public Customer getDetailsOfCustomer(Integer customerId, Integer busId) throws Exception{
		Customer customer = new Customer();
		
		customer.setCustomerId(customerId);
		
		Bus bus = restTemplate.getForObject("http://localhost:3000/bus/getList/"+busId+"/"+customerId, Bus.class);
		
		customer.setBus(bus);
		for(Customer customerIndex: customerList) {
			if(customerIndex.getCustomerId().equals(customerId)) {
				customer.setCustomerName(customerIndex.getCustomerName());
				customer.setMobileNumber(customerIndex.getMobileNumber());
			}
		}
		
		return customer;
		
	}
	

	
	
}

package com.test.customerservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.customerservice.model.Customer;
import com.test.customerservice.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private CustomerService service;
	
	
	
	@GetMapping("/getCustomer/{customerId}/{busId}")
	public Customer getCustomerDetails(@PathVariable(name = "customerId", required=true)Integer customerId,@PathVariable(name = "busId", required=true) Integer busId) throws Exception{
		
		return service.getDetailsOfCustomer(customerId, busId);
		
	}
	
	

	
}
